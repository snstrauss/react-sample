/**
 * this is the basic React boilerplate.
 * 'React' HAS to be imported, for the code to work, even though it's not used in the code strictly.
 * 'ReactDOM is the magic that takes react and renders it to html.
 * (check out react-native, reactVR, react-native-windows, etc....)
 */
import React from 'react';
import ReactDOM from 'react-dom';

/**
 * we import App - that is the react app root, 
 * that will be mounted on the only necessary piece of real DOM
 */
import App from './src/components/_main/main.comp';

/**
 * this line mounts the App component on the DOM.
 */
ReactDOM.render(<App />, document.getElementById('root'))