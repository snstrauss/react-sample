import React from 'react';
import './child.css'

/**
 * sometimes, we don't need to use a full-on component.
 * some components only need to be 'representational components'
 * these only show data, but has no logic or state of their own.
 * 
 * for these component we may use a simple function, instead of a class that extends React.Component
 */
const Child = (props) => {
    /*
        notice: simple, function only, representational components dont have 'this'.
        they get their props as an argument.
    */    
    const computedStyle = {
        borderWidth: (props.idx * 2) + 'px',
        borderColor: props.color
    }
    
    return (    
        /**
         * style can be a dynamic value.
         * we can use JS to create a dynamic style, and just pass it to the element.
         */
        /**
         * this 'onClick' is a bit special.
         * this onClick has a function inside it, and this function is the one calling the method passed to it on the 'props'.
         * this is done in order to pass data from the Child to the method that was passed to Child as a prop.
         */
        <div style={computedStyle} className="child" onClick={() => {
                    
            props.click(props.user)
        }}>
            i am Child <br />
            name: {props.user.name}
        </div>
    )
}

export default Child;