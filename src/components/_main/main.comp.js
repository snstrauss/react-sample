/**
 * import for the React lib, and css file
 */
import React from 'react';
import './main.style.css';

/**
 * importing the Parent component
 */
import Parent from '../parent/parent';

class App extends React.Component {
    /**
     * in react,
     * every component HAS to implement a 'render' method.
     * this method HAS to return at least some element.
     * 
     * in this case, we return some basic html,
     * also, we are using the <Parent /> component.
     * 
     * notice that we pass a 'title' property to the <Parent /> component.
     */
    render(){
        return (
            <div className="main_app">
                <h2>title</h2>
                <span>things</span>
                <Parent title="peantman"></Parent>
            </div>
        )
    }
}

/**
 * each component HAS to 'export defalut ComponentName'.
 * this is js, not react. every file is it's own module,
 * and won't have access to anything that's not imported.
 * the export will make it possible for other parts to use this component
 */
export default App;