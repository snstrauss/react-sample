import React, { Component } from 'react';
import './parent.css';
import Child from '../child/child'

class Parent extends Component {

    /**
     * a component can have a constructor,
     * where you will instantiate all kinds of thing, like the component state.
     */
    constructor(){
        super();

        /**
         * the state (reserved name) will hold the data of the component.
         * use state only if needed... only if it has to remember changes.
         */
        this.state = {
            counter: 0,
            users: [],
            chosen: ''
        }

        /**
         * this little thing is a must.
         * because of JS's weird 'this' behaviour,
         * using this trick will make the right 'this' 
         * to be used inside the method.
         */
        this.clicked = this.clicked.bind(this);
    }

    /**
     * react has some 'life cycle methods'.
     * those are methods that the component CAN implement,
     * and are used as hooks to do some actions in different times in the component render cycle.
     * 
     * this specific method will run right after the component is added to the DOM, and before the render.
     */
    componentDidMount(){
        /**
         * fetch is native js. it's used to make REST calls.
         */
        fetch('https://jsonplaceholder.typicode.com/users')
        .then(res => res.json())
        .then((json) => {
            
            /**
             * this is the real magic of react.
             * this.setState is the ONLY way to change the state of the component.
             * changing the state of the component is how you make the component to render again.
             * 
             * in this case, we give this.state.users a new value, one that we got from the 'fetch' call.
             */
            this.setState({
                users: json
            })
        })
        
    }

    /**
     * this is a method of the Parent component.
     * it will receive a user and will update this.state.chosen with the user's name
     */
    clicked(user){
        this.setState({
            chosen: user.name
        })
    }

    getColor(){
        return "#"+((1<<24)*Math.random()|0).toString(16)
    }

    render(){
        /**
         * important about render():
         * The only way to make a component render again are: 
         *  1. make the component update itself, with this.setState
         *  2. the component is updated by it's parent, by changing of it's props.
         */
        return (
            <div className="parent">
                i am parent <br />
                chosen: {this.state.chosen}
                {/* 
                    in order to use data that was passed as an 'attribute' on the Component,
                    we use 'props'.
                    in order to use JS inside the JSX, we use {} (even this comment is inside {}, and the comment syntax is JS)
                */}
                <h2>{this.props.title}</h2>
                
                {/* 
                    another example for using JS in JSX.
                    inside the following {} we use a ternary operator
                    in order to render the children ONLY if there is content inside this.state.users.
                    this is the equivilant of ng-if
                */}

                {
                    this.state.users.length
                    ?
                    /**
                     * here we use JS 'map' in order to take items in an array, and map them to a list of whatever i want
                     * in this case, we use the data from the array item and pass them to a 'Child' component
                     */
                    this.state.users.map((user, idx) => {
                        
                        debugger;
                        let color = 'red';
                        if(idx % 3 === 0){
                            color = "#"+((1<<24)*Math.random()|0).toString(16)
                        }
                        
                        // in order to pass JS data to components, we use {} instead of "". the "" is only for strings.
                        return (
                            <Child user={user} click={this.clicked} color={color} idx={idx}/>
                        )
                    })
                    :
                    'dont have users yet'
                }
            </div>
        )
    }
}

export default Parent;